file(REMOVE_RECURSE
  "CMakeFiles/seth_test.dir/main.cpp.o"
  "CMakeFiles/seth_test.dir/seth/src/activations/Activations.cpp.o"
  "CMakeFiles/seth_test.dir/seth/src/core/Core.cpp.o"
  "CMakeFiles/seth_test.dir/seth/src/core/Data.cpp.o"
  "CMakeFiles/seth_test.dir/seth/src/core/Layer.cpp.o"
  "CMakeFiles/seth_test.dir/seth/src/core/Model.cpp.o"
  "CMakeFiles/seth_test.dir/seth/src/initializer/RandomNorm.cpp.o"
  "CMakeFiles/seth_test.dir/seth/src/metrics/Metrics.cpp.o"
  "CMakeFiles/seth_test.dir/seth/src/nets/DenseNet.cpp.o"
  "CMakeFiles/seth_test.dir/seth/src/nets/InceptionX.cpp.o"
  "CMakeFiles/seth_test.dir/seth/src/nets/MobileNet.cpp.o"
  "CMakeFiles/seth_test.dir/seth/src/nets/ResNet50.cpp.o"
  "CMakeFiles/seth_test.dir/seth/src/nets/SqueezeNet.cpp.o"
  "CMakeFiles/seth_test.dir/seth/src/nets/VGG16.cpp.o"
  "CMakeFiles/seth_test.dir/seth/src/nn/Conv.cpp.o"
  "CMakeFiles/seth_test.dir/seth/src/nn/ConvDeformable.cpp.o"
  "CMakeFiles/seth_test.dir/seth/src/nn/LSTM.cpp.o"
  "CMakeFiles/seth_test.dir/seth/src/nn/Neural.cpp.o"
  "CMakeFiles/seth_test.dir/seth/src/nn/RNN.cpp.o"
  "CMakeFiles/seth_test.dir/seth/utils/random.cpp.o"
  "CMakeFiles/seth_test.dir/seth/utils/t_log.cpp.o"
  "CMakeFiles/seth_test.dir/seth/utils/t_os.cpp.o"
  "CMakeFiles/seth_test.dir/seth/utils/t_string.cpp.o"
  "seth_test.pdb"
  "seth_test"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/seth_test.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
