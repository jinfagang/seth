#include <iostream>
#include "eigen3/Eigen/Dense"
#include "eigen3/unsupported/Eigen/CXX11/Tensor"


#include "seth/include/nn/Neural.h"
#include "seth/include/core/Model.h"
#include "seth/include/core/Data.h"
#include "seth/include/activations/Activations.h"



using namespace std;
using namespace seth;


int main()
{
    kick_it_random();

    NDArray<double> x(23, 34, 3);
    cout << x << endl;

    NDArray<double> y(4, 3);
    cout << y << endl;


//    Model model;
//    model.add(Neural(x, 6, 10));
//    model.add(Neural(10, 4));
//    model.add(Neural(4, 1));
//    model.detailLayers();
//
//    // model.feed(x, y);
//    model.train(20, 10, Metrics::MSE);
}
