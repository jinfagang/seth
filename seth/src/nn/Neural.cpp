//
// Created by jintian on 8/18/17.
//


#include <iostream>

#include "../../utils/colors.h"
#include "../../include/nn/Neural.h"

using namespace std;
using namespace colors;


/**
 * implementation of Neural
 * @param x
 * @param in_size
 * @param out_size
 */
seth::Neural::Neural(Eigen::MatrixXd x, int in_size, int out_size) {
    assert(x.cols() == in_size && "x.cols must equals in_size.");

    kick_it_random();
    MatrixXd W = MatrixXd::Random(in_size, out_size);
    this->W = W;

    MatrixXd bias = MatrixXd::Random(x.rows(), out_size);
    this->bias = bias;
    this->numSamples = (int) x.rows();

    this->in_size = in_size;
    this->out_size = out_size;

}

seth::Neural::Neural(int in_size, int out_size) {
    // another init method for Neural Layer
    kick_it_random();
    MatrixXd W = MatrixXd::Random(in_size, out_size);
    this->W = W;
    // in this initialize we don't initialize bias here
    // initialize it in Model class

    this->in_size = in_size;
    this->out_size = out_size;

}

seth::Neural::~Neural() {

}


seth::Neural::Neural(Eigen::MatrixXd x, int size) {
    MatrixXd W = MatrixXd::Random(x.rows(), size);
    this->W = W;

    MatrixXd bias = MatrixXd::Random(x.rows(), size);
    this->bias = bias;
    this->numSamples = (int) x.rows();

    this->size = size;
}
seth::Neural::Neural(int size) {

}
