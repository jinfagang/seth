//
// Created by jintian on 8/21/17.
//

#include "../../include/metrics/Metrics.h"



double seth::MSE(Eigen::MatrixXd y, Eigen::MatrixXd predictY) {
    assert(y.cols() == predictY.cols() == 1 && "mse metric y and predict y must has 1 col.");
    Eigen::MatrixXd diff = y - predictY;
    // convert diff to vector first
    Eigen::VectorXd diffVec(Eigen::Map<Eigen::VectorXd>(diff.data(), diff.cols()*diff.rows()));
    double loss = diffVec.dot(diffVec) / diffVec.rows();
    return loss;
}

double seth::CrossEntropy(Eigen::MatrixXd y, Eigen::MatrixXd predictY) {
    return 0.9;
}

double seth::metric(Metrics type, Eigen::MatrixXd y, Eigen::MatrixXd predictY) {
    double loss;
    switch (type) {
        case seth::Metrics::MSE:
            loss = seth::MSE(y, predictY);
            break;
        case seth::Metrics::CrossEntropy:
            loss = seth::CrossEntropy(y, predictY);
            break;
    }
    return loss;
}