//
// Created by jintian on 8/18/17.
//
// Here is the class of base layer

#include "../../include/core/Layer.h"


seth::Layer::Layer() {
    this->name = "default layer";
}

string seth::Layer::getLayerName() {

    switch (this->type) {
        case seth::LayersType::Neural:
            return "Neural";
        case seth::LayersType::Convolution:
            return "Convolution";
        case seth::LayersType::ConvolutionDeformable:
            return "ConvolutionDeformable";
        case seth::LayersType::RNN:
            return "RNN";
        case seth::LayersType::LSTM:
            return "LSTM";
    }
    return "Neural";

}

seth::Layer::~Layer() {

}