//
// Created by jintian on 8/19/17.
//

#include "../../include/core/Model.h"
#include "../../utils/colors.h"



seth::Model::Model() {

}
seth::Model::~Model() {

}

void seth::Model::setModelPath(string path) {
    this->modelPath = path;
}


/**
 * this method feed data into net
 * @param x
 * @param y
 */
void seth::Model::feed(Eigen::MatrixXd x, Eigen::MatrixXd y) {

    this->x = x;
    this->y = y;
    this->isFeedData = true;
}

/**
 * train on epochs, and save periodically
 * @param epochs
 * @param savePeriod
 */
void seth::Model::train(int epochs, int savePeriod, seth::Metrics type) {
   if (this->isFeedData) {

       for (int i = 0; i < epochs; ++i) {
           // every epoch do follow things:
           // 1. forward get predict y, calculate error;
           // 2. backward update every W in every layer

           cout <<colors::bold << colors::yellow << "Epoch " << i << endl << colors::reset;
           Eigen::MatrixXd predictY = this->forward();

           // using MSE as default metric
           double loss = seth::metric(type, this->y, predictY);
           Log("loss " + to_string(loss));
           this->backPropagation();

       }
   } else {
       cout << "you haven't feed data, this is debug mode.\n";
   }

}


void seth::Model::add(seth::Layer layer) {
    this->layers.push_back(layer);
    // except add a new layer, add method will
    // initial the last layer's bias
    if (this->layers.size() > 1) {
        // indicates it's not the first layer
        int out_size = this->layers.back().out_size;

        // init random with this layer bias
        srand((unsigned int) time(0));
        this->layers.back().bias = Eigen::MatrixXd::Random(this->numSamples, out_size);

    } else if (this->layers.size() == 1) {
        // indicates this is the first layer, get the numSamples information
        this->numSamples = layer.numSamples;
    }
}

Eigen::MatrixXd seth::Model::inference(MatrixXd x) {
    // inference the result, also called forward process
    // forward process to calculate the predict y

}

Eigen::MatrixXd seth::Model::forward() {
    // forward process
    if (this->isFeedData) {

        Eigen::MatrixXd middleValue = this->x;
        for (int i = 0; i < this->layers.size(); ++i) {
            MatrixXd tmp = middleValue * this->layers[i].W;
            middleValue = tmp + this->layers[i].bias;
            // activate middle value
            middleValue = seth::activate(this->layers[i].activation, middleValue);
        }
        return middleValue;
    } else {
        cout << colors::red << "you may not feed data yet, "
                "can not perform forward operation.\n" << colors::reset;
    }
}

void seth::Model::backPropagation() {
    // back propagation to update weights
    // TODO: how to using loss to update weights??????? last question

}


void seth::Model::load(string modelPath) {
    // load the saved model and weights
}

void seth::Model::load() {

}

bool seth::Model::save(string modelPath) {
    // this method will save all layers weights and bias to local
}

bool seth::Model::save() {

}

void seth::Model::detailLayers() {
    // this method will print all layers in vector with detail message
    if (this->layers.size() >= 1) {
        for (int i=0; i < this->layers.size(); i++) {
            cout << colors::blue << "layer" << std::to_string(i) << ": ";
            cout << colors::green << this->layers[i].getLayerName() << ", ";
            cout << colors::reset;

            cout << "in size: ";
            cout << this->layers[i].in_size << ", ";
            cout << "out size: ";
            cout << this->layers[i].out_size << ", ";
            cout << "weights size: " << "[" << this->layers[i].W.rows()
                 << ", " << this->layers[i].W.cols() << "]" << ", ";
            cout << "bias size: " << "[" << this->layers[i].bias.rows()
                 << ", " << this->layers[i].bias.cols() << "]" << " " << endl;
            cout << colors::reset;
        }
    } else {
        cout << colors::red << "you may not add layers yet, nothing to print.\n" << colors::reset;
    }

}