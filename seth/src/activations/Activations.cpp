//
// Created by jintian on 8/21/17.
//

#include "../../include/activations/Activations.h"


double seth::ReLU(double a) {
    if (a >= 0) {
        return a;
    } else {
        return 0;
    }
}

double seth::TanH(double a) {
    return 2*seth::Sigmoid(2*a) - 1;
}

double seth::MaxOut(double a) {

}

double seth::Sigmoid(double a) {
    return 1.0 / (1.0 + std::exp(-a));
}

double seth::LeakyReLU(double a) {
    if (a >= 0) {
        return a;
    } else {
        return 0.2 * a;
    }
}

Eigen::MatrixXd seth::activate(seth::Activations type, Eigen::MatrixXd in_data) {
    Eigen::MatrixXd activated;
    switch (type) {
        case seth::Activations::Sigmoid:
            activated = in_data.unaryExpr(&seth::Sigmoid);
            break;
        case seth::Activations::ReLU:
            activated = in_data.unaryExpr(&seth::ReLU);
            break;
        case seth::Activations::TanH:
            activated = in_data.unaryExpr(&seth::TanH);
            break;
        case seth::Activations::LeakyReLU:
            activated = in_data.unaryExpr(&seth::MaxOut);
            break;
    }
    return activated;
}

