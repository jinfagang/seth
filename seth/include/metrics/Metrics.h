//
// Created by jintian on 8/21/17.
//

#ifndef SETH_METRICS_H
#define SETH_METRICS_H

#include "../core/Core.h"


// metrics contains various loss calculation methods

namespace seth{


    enum class Metrics{MSE, CrossEntropy};

    double metric(Metrics type, Eigen::MatrixXd y, Eigen::MatrixXd predictY);


    // various metric function define
    double MSE(Eigen::MatrixXd y, Eigen::MatrixXd predictY);
    double CrossEntropy(Eigen::MatrixXd y, Eigen::MatrixXd predictY);

}

#endif //SETH_METRICS_H
