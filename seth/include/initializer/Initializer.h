//
// Created by jintian on 8/18/17.
//

#ifndef SETH_INITIALIZER_H
#define SETH_INITIALIZER_H


namespace seth{

    enum Initializer{random_norm, gaussian_norm};


    class BaseInitializer{
        // base class of Initializer
    };
}
#endif //SETH_INITIALIZER_H
