//
// Created by JinTian on 03/11/2017.
//

#ifndef SETH_DATA_H
#define SETH_DATA_H

#include "eigen3/Eigen/Dense"
#include "eigen3/unsupported/Eigen/CXX11/Tensor"
#include "Core.h"

using namespace Eigen;
using namespace std;

// this class is the core data type in seth,
// as for the data is the ordinary type, it has a lot properties as numpy in python

namespace seth{

    template <class T>
    class NDArray{
    private:
        MatrixXd nd_2;
        Tensor<T, 3> nd_3;
        Tensor<T, 4> nd_4;
        Tensor<T, 5> nd_5;
        Tensor<T, 6> nd_6;


    public:
        seth::NDArray<T>* shape;
        int dim;

        // construct methods
        // currently only support 2-4 dimensions
        NDArray(int a1, int a2);
        NDArray(int a1, int a2, int a3);
        NDArray(int a1, int a2, int a3, int a4);
        NDArray(int a1, int a2, int a3, int a4, int a5);
        NDArray(int a1, int a2, int a3, int a4, int a5, int a6);


//        void transpose();
//        NDArray<T> random(int a1, int a2);
//        NDArray<T> random(int a1, int a2, int a3);
//        NDArray<T> random(int a1, int a2, int a3, int a4);

        // ostream& operator<< (ostream& os, NDArray nd);
        friend ostream& operator<< (ostream &os, const seth::NDArray<T> &nd) {
            if (nd.dim == 2)
            {
                os << nd.nd_2;
                return os;
            }
            else if (nd.dim == 3)
            {
                os << nd.nd_3;
                return os;
            } else if (nd.dim == 4) {
                os << nd.nd_4;
                return os;
            } else if (nd.dim == 5) {
                os << nd.nd_5;
                return os;
            }else if (nd.dim == 6) {
                os << nd.nd_6;
                return os;
            }
        }
    };

    template <class T>
    NDArray<T>::NDArray(int a1, int a2) {
        this->nd_2(a1, a2);
        this->dim = 2;
    }
    template <class T>
    NDArray<T>::NDArray(int a1, int a2, int a3) {
        Tensor<T, 3> tensor(a1, a2, a3);
        this->nd_3 = tensor;
        this->dim = 3;
    }
    template <class T>
    NDArray<T>::NDArray(int a1, int a2, int a3, int a4) {
        Tensor<T, 4> tensor(a1, a2, a3, a4);
        this->nd_4 = tensor;
        this->dim = 4;
    }
    template <class T>
    NDArray<T>::NDArray(int a1, int a2, int a3, int a4, int a5) {
        Tensor<T, 5> tensor(a1, a2, a3, a4, a5);
        this->nd_5 = tensor;
        this->dim = 5;
    }

    template <class T>
    NDArray<T>::NDArray(int a1, int a2, int a3, int a4, int a5, int a6) {
        Tensor<T, 6> tensor(a1, a2, a3, a4, a5, a6);
        this->nd_6 = tensor;
        this->dim = 6;
    }
}


#endif //SETH_DATA_H
