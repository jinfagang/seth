//
// Created by jintian on 8/18/17.
//

#ifndef SETH_BASE_H
#define SETH_BASE_H


#include <iostream>
#include <string>

#include "eigen3/Eigen/Dense"

#include "../activations/Activations.h"



using namespace Eigen;
using namespace std;


namespace seth{

    // basic layer, Neural, Convolution, ConvolutionDeformable must implement this
    // basic layer properties

    enum class LayersType {
        Convolution,
        ConvolutionDeformable,
        RNN,
        LSTM,
        Neural
    };

    class Layer{

    public:
        string name;
        seth::LayersType type = seth::LayersType::Neural;


        MatrixXd W;
        MatrixXd bias;


        int numSamples;
        int in_size;
        int out_size;

        // the default activation is sigmoid
        seth::Activations activation = seth::Activations::ReLU;

        string getLayerName();

        Layer();
        ~Layer();



    };

}


#endif //SETH_BASE_H
