//
// Created by jintian on 8/19/17.
//

#ifndef SETH_NET_H
#define SETH_NET_H

// Net will be a class
#include <iostream>
#include <string>
#include <vector>
#include "eigen3/Eigen/Dense"

#include "Layer.h"
#include "Core.h"

#include "../activations/Activations.h"
#include "../metrics/Metrics.h"


namespace seth{

    class Model{
    public:
        Model();
        ~Model();

        // value property of Net
        Eigen::MatrixXd value;
        std::string modelPath;

        std::vector<Layer> layers;

        // data input and output
        Eigen::MatrixXd x;
        Eigen::MatrixXd y;
        // this is the actually the x.shape[0]
        int numSamples;



        // add a layer and add directly inside this model
        void add(seth::Layer layer);

        // set model save path
        void setModelPath(string path);

        // feed data into net
        void feed(Eigen::MatrixXd x, Eigen::MatrixXd y);

        // train on data
        void train(int epochs, int savePeriod, seth::Metrics type);

        // inference the result y
        Eigen::MatrixXd inference(MatrixXd x);


        // load the model and weights
        void load(string modelPath);
        void load();

        bool save(string modelPath);
        bool save();


        // utils functions
        void detailLayers();

    private:
        // check if feed data into model
        bool isFeedData = false;

        Eigen::MatrixXd forward();
        void backPropagation();




    };
}


#endif //SETH_NET_H
