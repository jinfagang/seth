//
// Created by jintian on 8/21/17.
//

#ifndef SETH_ACTIVATIONS_H
#define SETH_ACTIVATIONS_H


#include <iostream>
#include <string>
#include "../core/Core.h"


using namespace std;

namespace seth{

    enum class Activations{ReLU, TanH, Sigmoid, LeakyReLU, MaxOut};


//    template <typename Element>
//    Element activate(seth::Activations type, Element in_data);

    Eigen::MatrixXd activate(seth::Activations type, Eigen::MatrixXd in_data);

    double ReLU(double a);

    double TanH(double a);

    double Sigmoid(double a);

    double MaxOut(double a);

    double Sigmoid(double a);

    double LeakyReLU(double a);


}
#endif //SETH_ACTIVATIONS_H
