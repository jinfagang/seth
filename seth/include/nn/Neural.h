//
// Created by jintian on 8/19/17.
//

#ifndef SRC_SETH_NN_H
#define SRC_SETH_NN_H

#include <iostream>
#include <string>

#include "../core/Layer.h"
#include "../core/Core.h"


using namespace std;

namespace seth {

    class Neural:public Layer{
        // Neural inherit from Layer

    public:
        // this is only Neural persist
        int size;

        Neural(Eigen::MatrixXd x, int in_size, int out_size);
        Neural(int in_size, int out_size);



        // these 2 method, using only one param indicates size of neuron
        // these 2 method not in use, cause W will also inference from model
        // which can be really un-elegant and dirty
        Neural(Eigen::MatrixXd x, int size);
        Neural(int size);
        ~Neural();

    };

}

#endif //SRC_SETH_NN_H
