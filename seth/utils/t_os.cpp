//
// Created by jintian on 7/14/17.
//
// this library provider some function to
// judge a directory is exist or not
#include <sys/types.h>
#include <sys/stat.h>
#include "t_os.h"
#include <iostream>
#include <vector>
#include <string>
#include <dirent.h>
#include <valarray>
#include "t_string.h"

#include "t_log.h"


struct stat info;
using namespace std;

bool t_os::exists(char *path) {
    // if path exists, return true else return false
    if (stat(path, &info) != 0) {
        // path not exist
        return false;
    } else {
        // this is a file
        return true;
    }
}

bool t_os::isdir(char *path) {
    // judge is directory or file, if dir return true, if file return false
    if (stat(path, &info) != 0) {
        // path not exist
        return false;
    } else if (info.st_mode & S_IFDIR) {
        // this is a directory
        return true;
    } else {
        return false;
    }

}

bool t_os::isfile(char *path) {
    // return a path is file or not
    if (stat(path, &info) != 0) {
        // path not exist
        return false;
    } else if (info.st_mode & S_IFDIR) {
        // this is a directory
        return false;
    } else {
        return true;
    }
}

vector<string> t_os::list_files(char *path) {
    DIR *dp;
    struct dirent *dirP;
    vector<string> files;
    if ((dp = opendir(path)) == NULL) {
        cout << "dir not exist." << endl;
    }

    while ((dirP = readdir(dp)) != NULL) {
        if (dirP->d_type == DT_REG) {
            files.push_back(path + string("/") + string(dirP->d_name));
        }
    }

    closedir(dp);
    return files;
}

vector<string> t_os::list_dirs(char *path) {
    DIR *dp;
    struct dirent *dirP;
    vector<string> files;
    if ((dp = opendir(path)) == NULL) {
        cout << "dir not exist." << endl;
    }

    while ((dirP = readdir(dp)) != NULL) {
        if (dirP->d_type == DT_REG) {
            files.push_back(path + string("/") + string(dirP->d_name));
        }
    }

    closedir(dp);
    return files;
}

string t_os::join(char *path, string filename) {
    // path maybe /home/jin/doc1 or /home/jin/doc1/
    // make sure drop the last '/'
    string path_string = string(path);
    string joiner = "/";
#ifdef __WIN32
    joiner = "\\";
#endif

#ifdef _UNIX
    // unix machine
    joiner = "/";
#endif

#ifdef __APPLE__
    joiner = "/";
#endif
    vector<string> split_r;

    SplitString(path_string, split_r, joiner);
    StripString(split_r, "");
    string path_s = JoinString(joiner, split_r);
    string joined_path = path_string + joiner + filename;
    return joined_path;
}

string t_os::parent_path(char *path) {
    // get dir name of a path
    string joiner = "/";
#ifdef __WIN32
    joiner = "\\";
#endif

#ifdef __unix__
    // unix machine
    joiner = "/";
#endif

#ifdef __APPLE__
    joiner = "/";
#endif
    LogInfo("get parent path, this is original " + (string) path);
    vector<string> split_r;
    SplitString(path, split_r, joiner);
    vector<string> split_drop_file_name(split_r.begin(), split_r.end() - 1);
    string dir = JoinString(joiner, split_drop_file_name);
    LogInfo("to see is the parent path is right. " + dir);
    return dir;
}

string t_os::filename(char *path){

}

void t_os::mkdir(char *path) {

}
void t_os::makedirs(char *path) {

}