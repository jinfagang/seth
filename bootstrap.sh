# !/usr/bin/bash

# =========== install eigen to local ==========
cd ~/Downloads
wget http://bitbucket.org/eigen/eigen/get/3.3.4.tar.bz2
sudo tar -xvjf ~/Downloads/3.3.4.tar.bz2  -C /usr/local/include
sudo mkdir /usr/local/include/eigen3
sudo mv /usr/local/include/eigen-eigen-*/Eigen  /usr/local/include/eigen3
sudo mv /usr/local/include/eigen-eigen-*/unsupported /usr/local/include/eigen3
