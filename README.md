# Seth
> Simplest Ever - The Seth.

<img src="http://ofwzcunzi.bkt.clouddn.com/RfwKwU4ZQzu9vYMr.png" width="100%" height="auto" align="center">

## Synopsis

**Seth** is a DeepLearning library build with concept of pure, simple and clearness. We can build a network with Multiple Layer like this in C++:

```
NDArray<double> x(4, 3);
NDArray<double> y(4);

Model model;
model.add(Neural(x, 3, 10));
model.add(Neural(10, 4));
model.add(Neural(4, 1));

model.feed(x, y);
model.train(20, 10, Metrics::MSE);
```

Amazing! We are just build a network in 8 line codes!! **Seth** have the following beautiful design features:

- Seth has only one dependency on eigen;
- Seth  has only one data type of NDArray but can not any dimension of arrays;
- Seth has a very clean and simple API of build neural network;
- Seth can be build in an executable file and your network can run anywhere.

## Install

To install seth is very simple, the whole repo is just an project, and the source code is in `seth/seth/src`, you can clone it:

```
git clone https://gitlab.com/jinfagang/seth you_project_name
```

For various platform there is only one thing to do is install eigen from apt or brew:

```
sudo apt install libeigen-dev
brew install eigen3
```

Then just run:

```
mkdir build
cd build
cmake ..
make all -j8
./seth_test
```

and your will see **seth** is training on a mimick data.



## Copyright

**seth** was build by *Jinfagang* and maintained by him. You may using it under Apache License. If you have any question about seth, you can find me via wechat: `jintianiloveu`.